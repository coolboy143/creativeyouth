<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Register for Membership</h4>
    </div>
    <div class="modal-body">
        <form action="{{ url ('register') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="form-group">
                    <label for="Email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="Password">Password:</label>
                    <input type="pass" class="form-control" id="pass" name="pass">
                </div>
                <div class="form-group">
                    <label for="dob">DOB:</label>
                    <input type="text" class="form-control" id="dob" name="dob">
                </div>
                <div class="form-group">
                    <label for="gender">Gender:</label>
                    <input type="text" class="form-control" id="email" name="gender">
                </div>
                <div class="form-group">
                    <label for="contact">Contact No:</label>
                    <input type="text" class="form-control" id="contact" name="contact">
                </div>
            </div>

            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

