<?php

namespace App\Http\Controllers;

use App\User;
use App\UserModel\UserProfile;
use App\UserModel\UserRole;
use Illuminate\Http\Request;

class CommunityController extends Controller
{
    //

    public function verifyGuestToMember(Request $request){
        try{
            $changeRole = UserRole::where('userid', '=', $request->get('userid'))
                ->update([
                    'roleid' => 3//$request->get('roleid);
                ]);

            $changeActiveStatus = User::where('id', '=', $request->get('userid'))
                ->update([
                    'statusid' => 1//$request->get('roleid);
                ]);

            if ($changeActiveStatus) {
                return response()->json(['message' => 'You are Activated and role changed']);
            } else {
                return response()->json(['message' => 'Something Went Wrong']);
            }
        } catch (\PDOException $e) {
            return response()->json(['message' => 'Something Went Wrong, Error: ' . $e->getMessage()]);
        }
    }
}
