<?php

namespace App\Http\Controllers;
use App\User;


use App\Models\UserProfile;
use App\Models\UserRole;
use App\Models\Profession;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;


class RegisterController extends Controller
{
    public function index()
    {
        try{
            $data =User::all();
            if(count($data) == 0){
                return response()->json(['message' => 'Sorry !!! No data found']);
            }else {
//                $da=$data->firstname;
                return response()->json(['datas' => $data]);
            }
        }catch(\Exception $e)
        {
            return response()->json(['message' => 'Something Went Wrong, Error: '. $e->getMessage()]);
        }
    }

    public function register(Request $request)
    {
        DB::beginTransaction();

        try {
//            $data = new User;
//            $data->Email = $request->input('email');
//            $data->Password = bcrypt($request->input('password'));
//            $data->DOB = $request->input('dob');
//            $data->Gender = $request->input('gender');
//            $data->Contact = $request->input('contact');
//            $data->statusid = 2;
//            $data->save();



            $userObj = User::create([
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'statusid' => 2,
            ]);

//            print_r($userObj->id);die();

            $user_id = $userObj->id;

            $userRoleObj = UserRole::create([
                'userid' => $user_id,
                'roleid' => 4
            ]);

//                        print_r($userRoleObj->roleid);die();

            $userProfile = UserProfile::create([
               'DOB' => $request->get('dob'),
                'gender' => $request->get('gender'),
                'first_name' => $request->get('firstname'),
                'last_name' => $request->get('lastname'),
                'userid' => $user_id
            ]);

            $userProfesssion=Profession::create([
                'userid'=> $user_id,
                'type'=> $request->get('type'),
                'detail'=>$request->get('detail')
            ]);
            DB::commit();


            if ($userObj) {
                return response()->json(['message' => 'You are Register to CY', 'data' => $userObj]);
            } else {
                return response()->json(['message' => 'Something Went Wrong']);
            }
        } catch (\PDOException $e) {

           DB::rollback();
           return response()->json(['message' => 'Something Went Wrong, Error: ' . $e->getMessage()]);
        }
    }

    public function view($id)
    {
        try{
            $result = User::find($id);

            return response()->json($result);

        }
        catch(\Exception $e)
        {
            print_r($e);
        }
    }

    public function delete($id)
    {
        try{
            $data = User::find($id);
            $data->delete();
            return response()->json(['message' => 'You are removed from CY member']);

        }
        catch(\Exception $e)
        {
            print_r($e);
        }
    }

    public function updateUser(Request $request, $id)
    {
        try {
            $data = User::find($id);

            $data->email = $request->input('email');
            $data->password = bcrypt($request->input('password'));



            $data->save();

            if ($data->save()) {
                return response()->json(['message' => 'data updated ', 'data' => $data]);
            }

        } catch (\Exception $e) {
            return $e;
        }
    }

    public function updateProfession(Request $request, $id)
    {
        try {
            $data = Profession::find($id);

            $data->type = $request->input('type');
            $data->detail = $request->input('detail');

            $data->save();

            if ($data->save()) {
                return response()->json(['message' => 'data updated ', 'data' => $data]);
            }

        } catch (\Exception $e) {
            return $e;
        }
    }
}
