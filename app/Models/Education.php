<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    //
    protected $table = 'education';

    public function User()
    {
        return $this->belongsTo('App\User','userid', 'id');
    }
}
