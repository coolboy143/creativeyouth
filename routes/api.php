<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*RegisterController*/

Route::get('list', 'RegisterController@index');
Route::post('register','RegisterController@register');
Route::get('/view/{id}', 'RegisterController@view');
Route::get('/delete/{id}', 'RegisterController@delete');
Route::post('/update/user/{id}','RegisterController@updateUser');
Route::post('/update/profession/{id}','RegisterController@updateProfession');

/*CommunityController*/
Route::post('/verify/user', ['uses' => 'CommunityController@verifyGuestToMember']);

//Route::get();